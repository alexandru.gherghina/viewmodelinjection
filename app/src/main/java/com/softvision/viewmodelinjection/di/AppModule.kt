package com.softvision.viewmodelinjection.di

import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Module

@AssistedModule
@Module(includes = [ActivityModule::class, ViewModelModule::class, AssistedInject_AppModule::class])
class AppModule