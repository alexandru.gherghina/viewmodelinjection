package com.softvision.viewmodelinjection.di

import com.softvision.viewmodelinjection.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    internal abstract fun contributesMainActivity(): MainActivity
}