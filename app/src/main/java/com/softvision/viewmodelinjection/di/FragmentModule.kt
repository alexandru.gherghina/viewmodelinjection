package com.softvision.viewmodelinjection.di

import com.softvision.viewmodelinjection.ui.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMainFragment(): MainFragment
}