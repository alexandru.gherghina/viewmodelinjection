package com.softvision.viewmodelinjection.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.softvision.viewmodelinjection.AssistedViewModelFactory
import com.softvision.viewmodelinjection.R
import com.softvision.viewmodelinjection.di.Injectable
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class MainFragment : DaggerFragment(), Injectable {

    companion object {
        fun newInstance() = MainFragment()
    }

    @Inject
    lateinit var viewModelFactory: MainViewModel.Factory

    private lateinit var viewModel: MainViewModel

    private val data = (Math.random() * 1000).toInt()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, AssistedViewModelFactory {
            viewModelFactory.create(data)
        }).get(MainViewModel::class.java)

        message.text = "Number: ${viewModel.data}"
    }

}
