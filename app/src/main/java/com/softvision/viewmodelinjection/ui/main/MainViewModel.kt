package com.softvision.viewmodelinjection.ui.main

import androidx.lifecycle.ViewModel
import com.softvision.viewmodelinjection.SomeDep
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject

class MainViewModel @AssistedInject constructor(
    @Assisted val data: Int,
    dep: SomeDep
) : ViewModel() {

    @AssistedInject.Factory
    interface Factory {
        fun create(data: Int): MainViewModel
    }
}
